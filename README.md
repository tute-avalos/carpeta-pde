# Programación de Dispositivos Electrónicos

## Descripción

Este es un libro de cátedra para la materia de 5to año de la modalidad Técnico Profesional en Electrónica del 
nivel medio de enseñanza **Programación de Dispositivos Electrónicos** de la Ciudad Autónoma de Buenos Aires,
Argentina. Por lo tanto está orientado a un público adolescente de entre 16 y 19 años que tengan conocimiento 
en electrónica digital es decir (que hayan cursado la materia Técnicas Digitales del 4to año de la modalidad).

El mismo está siendo escrito utilizando solamente herramientas de Software Libre. Totalmente escrito en LaTeX,
utilizando PDFLaTex.

## Objetivo

El libro debe abarcar toda la teoría y práctica referida a la programación de microcontroladores, desde su
funcionamiento más elemental a la programación de sus periféricos internos y algunos externos populares. No se
debe perder de vista que los principales receptores del material son adolescentes, por lo tanto el texto debe
mantener un lenguaje accesible y una profundidad _acorde_ a los conocimientos que manejan.

### Conocimientos previos

Como se mencionó, para manejar el material propuesto, es necesario tener conocimientos de Técnicas Digitales.
Es decir, manejar todos los tipos de compuertas básicas, circuitos combinacioneales típicos, circuitos sincrónicos,
memorias, ALU, familias lógicas, etc. Pero también se deben tener conocimientos de programación estructurada
preferentemente en C o alguna experiencia de programación (p.e. Arduino). De otra manera la apropiación del 
contenido se hará muy dificultosa para el lector.

### Contenido

El contenido a abarcar es el siguiente:

*   Parte I: Introducción
    *   Unidad 0: Registros, Memorias y ALU
    *   Unidad 1: Estructura de una Computadora
    *   Unidad 2: Introducción a la Programación de µC
*   Parte II: Periféricos
    *   Unidad 3: E/S de Propósito General
    *   Unidad 4: Interrupciones
    *   Unidad 5: Temporizadores y Contadores
    *   Unidad 6: Señales analógicas y digitales
    *   Unidad 7: Periféricos de comunicación
    *   Unidad 8: Almacenamiento no volátil
    *   Unidad 9: Periféricos externos
*   Parte III: Software y algoritmos
    *   Unidad 10: Técnicas de desarrollo
    *   Unidad 11: Recursos de programación
    *   Unidad 12: Otros dispositivos programables

## Aclaraciones

Este es un trabajo en proceso, el orden de los capítulos, el nombre y los mismos en sí pueden cambiar o ser 
elminiados. Además que se invita a cualquiera que quiera colaborar en la confección de este libro es más que
bienvenido a hacerlo.

